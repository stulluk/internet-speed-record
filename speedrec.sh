#!/bin/bash

set -euo pipefail

RECFILE="/tmp/internet-speed-record.txt"

printf "Starting speed record...\n"
	
CURDATE=$(date +'%d.%m.%Y-%H.%M.%S')
printf "\n-----------------------" | tee -a ${RECFILE}
printf "Test Date: %s\n" "${CURDATE}" | tee -a ${RECFILE}
speedtest | tee -a ${RECFILE}

